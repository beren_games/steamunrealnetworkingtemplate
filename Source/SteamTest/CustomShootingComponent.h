// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "FP_FirstPerson/FP_FirstPersonCharacter.h"
#include "ArrowProjectile.h"
#include "CustomShootingComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChargingShot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChargingShotEnd);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class STEAMTEST_API UCustomShootingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCustomShootingComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	ACharacter* OwnerCharacter = nullptr;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class AArrowProjectile> ProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shooting)
		float MaxShootForce = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shooting)
		float  MinimumShootForce = 500;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shooting)
		float ShootIncrement = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shooting)
		FVector SpawnSize = FVector(1);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shooting)
		FVector SpawnOffset = FVector(0);


	UFUNCTION(BlueprintCallable, Category = "Shooting")
		void ChargeShot();

	UFUNCTION(BlueprintCallable, Category = "Shooting")
		void FireShot();

	UFUNCTION(BlueprintCallable, Category = "Shooting")
		void CancelShot();


	bool HoldingFireButton = false;

	float CurrentShootForce = MinimumShootForce;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shooting)
	float ZoomTimeLerp = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shooting)
	FOnChargingShot OnChargingShot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shooting)
	FOnChargingShotEnd OnChargingShotEnd;
	
};
