// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine.h"
#include "FP_FirstPerson/FP_FirstPersonCharacter.h"
#include "CustomMovementComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWallRunningR);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWallRunningL);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNormalizeView);

enum class EPlayerState
{
	IDLE,
	WALL_CLIMBING,
	WALL_RUNNING_L,
	WALL_RUNNING_R

};


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class STEAMTEST_API UCustomMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	ACharacter* OwnerCharacter = nullptr;

	UCustomMovementComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DEBUG)
		bool DEBUGENABLED = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float DashForce;

	EPlayerState PlayerState = EPlayerState::IDLE;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		FRotator CurrentActorRotation = FRotator(0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float JumpForce = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float WallClimbingZVelocityTreshold = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float WallRunningZVelocityTreshold = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float WallRunForce = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float ClimbForce = 10000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float JumpAwayFromWallForce = 500.0f;

	UNavMovementComponent* NavMovementComponent = nullptr;
	UCharacterMovementComponent* CharacterMovementComponent = nullptr;

	bool HoldingDownSpace = false;
	int32 CurrentJumpCount = 0;
	const int32 MaxJumpCount = 2;
	float CurrentTime = 0.0f;
	bool CanDash = true;

public:

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void OnDash();

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void ResetJump();

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void ResetDash();

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void CustomJump();

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void ClimbWall();

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void RunWall(int32 direction);

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void JumpEnd();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		FOnWallRunningR OnWallRunningR;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		FOnWallRunningL OnWallRunningL;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		FNormalizeView OnNormalizeView;

};
