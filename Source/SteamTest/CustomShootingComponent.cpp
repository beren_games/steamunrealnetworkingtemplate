// Fill out your copyright notice in the Description page of Project Settings.

#include "CustomShootingComponent.h"

#define print(text,time) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, time, FColor::White,text)

// Sets default values for this component's properties
UCustomShootingComponent::UCustomShootingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCustomShootingComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerCharacter = Cast<ACharacter>(GetOwner());
	if (nullptr == OwnerCharacter) print("character not found!", 2);

	if (nullptr != OwnerCharacter)
	{

		OwnerCharacter->InputComponent->BindAction("Fire", IE_Pressed, this, &UCustomShootingComponent::ChargeShot);
		OwnerCharacter->InputComponent->BindAction("Fire", IE_Released, this, &UCustomShootingComponent::FireShot);
		OwnerCharacter->InputComponent->BindAction("AlternateFire", IE_Released, this, &UCustomShootingComponent::CancelShot);

	}

	
}


// Called every frame
void UCustomShootingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (HoldingFireButton)
	{

		if (CurrentShootForce < MaxShootForce)
		{
			ZoomTimeLerp += DeltaTime;
			CurrentShootForce += ShootIncrement;
			OnChargingShot.Broadcast();
		}
		else
		{
			CurrentShootForce = MaxShootForce;

			print(FString::SanitizeFloat(ZoomTimeLerp), DeltaTime);
		}

	}
	else
		ZoomTimeLerp = 0;
}


void UCustomShootingComponent::CancelShot()
{
	HoldingFireButton = false;
	CurrentShootForce = 0;
	OnChargingShotEnd.Broadcast();
}

void UCustomShootingComponent::ChargeShot()
{
	HoldingFireButton = true;
}

void UCustomShootingComponent::FireShot()
{
	if (!HoldingFireButton)
		return;

	HoldingFireButton = false;
	OnChargingShotEnd.Broadcast();
	FVector StartLocation;
	FVector temp;
	FRotator StartDirection;

	//GetWorld()->GetFirstPlayerController()->GetViewportSize(x, y);

	//StartLocation = FVector(x / 2, y / 2, 0);

	OwnerCharacter->GetActorEyesViewPoint(StartLocation, StartDirection);

	StartLocation = StartLocation + (StartDirection.Vector() * 100) + SpawnOffset;

	FTransform SpawnTransform = FTransform(StartDirection, StartLocation, SpawnSize);
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	FVector InitialVelocity = StartDirection.Vector() * CurrentShootForce;

	if (NULL != ProjectileClass)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			AArrowProjectile* projectile = World->SpawnActor<AArrowProjectile>(ProjectileClass, SpawnTransform, ActorSpawnParams);
			if (nullptr != projectile)
				projectile->GetProjectileMovement()->Velocity = InitialVelocity;
		}
	}
	CurrentShootForce = MinimumShootForce;
}
