// Fill out your copyright notice in the Description page of Project Settings.

#include "CustomMovementComponent.h"
#define print(text,time) if (GEngine && DEBUGENABLED) GEngine->AddOnScreenDebugMessage(-1, time, FColor::White,text)


// Sets default values for this component's properties
UCustomMovementComponent::UCustomMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCustomMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerCharacter = Cast<ACharacter>(GetOwner());
	if (nullptr == OwnerCharacter)print("character not found!", 2);

	if (nullptr != OwnerCharacter)
	{
		NavMovementComponent = Cast<AFP_FirstPersonCharacter>(GetOwner())->FindComponentByClass<UNavMovementComponent>();
		CharacterMovementComponent = Cast<AFP_FirstPersonCharacter>(GetOwner())->FindComponentByClass<UCharacterMovementComponent>();
		OwnerCharacter->InputComponent->BindAction("Jump", IE_Pressed, this, &UCustomMovementComponent::CustomJump);
		OwnerCharacter->InputComponent->BindAction("Dash", IE_Pressed, this, &UCustomMovementComponent::OnDash);
		OwnerCharacter->InputComponent->BindAction("Jump", IE_Released, this, &UCustomMovementComponent::JumpEnd);
	}
	
}

// Called every frame
void UCustomMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	CurrentTime = DeltaTime;
	FVector WallClimbRayStart = GetOwner()->GetActorLocation();
	FVector WallClimbRayEnd = GetOwner()->GetActorLocation() + (GetOwner()->GetActorForwardVector() * 300);
	FVector WallRunRayEndRight = GetOwner()->GetActorLocation() + ((GetOwner()->GetActorRightVector().RotateAngleAxis(-10, GetOwner()->GetActorUpVector())) * 300);
	FVector WallRunRayEndLeft = GetOwner()->GetActorLocation() + (GetOwner()->GetActorRightVector().RotateAngleAxis(10, GetOwner()->GetActorUpVector())  * -300);

	FHitResult hit;

	bool ClimbRay = GetWorld()->LineTraceSingleByChannel(hit, WallClimbRayStart, WallClimbRayEnd, ECC_Visibility);
	bool RunRayRight = GetWorld()->LineTraceSingleByChannel(hit, WallClimbRayStart, WallRunRayEndRight, ECC_Visibility);
	bool RunRayLeft = GetWorld()->LineTraceSingleByChannel(hit, WallClimbRayStart, WallRunRayEndLeft, ECC_Visibility);


	if (NavMovementComponent == nullptr)
	{
		print("navmovementcomponent not available", DeltaTime);
		return;
	}

	HoldingDownSpace = (GetWorld()->GetFirstPlayerController()->GetInputKeyTimeDown(FKey("Spacebar")) > 0.0);

	if (!NavMovementComponent->IsMovingOnGround() && HoldingDownSpace)
	{
		if (ClimbRay && (NavMovementComponent->Velocity.Z > WallClimbingZVelocityTreshold))
		{
			if (!RunRayRight && !RunRayLeft)
				PlayerState = EPlayerState::WALL_CLIMBING;

		}
		else if (RunRayRight && (NavMovementComponent->Velocity.Z > WallRunningZVelocityTreshold))
			PlayerState = EPlayerState::WALL_RUNNING_R;
		else if (RunRayLeft && (NavMovementComponent->Velocity.Z > WallRunningZVelocityTreshold))
			PlayerState = EPlayerState::WALL_RUNNING_L;
		else
			PlayerState = EPlayerState::IDLE;
	}
	else
		PlayerState = EPlayerState::IDLE;

	if (DEBUGENABLED) UKismetSystemLibrary::DrawDebugLine(GetWorld(), WallClimbRayStart, WallClimbRayEnd, FColor::Red, 0, 2);
	if (DEBUGENABLED) UKismetSystemLibrary::DrawDebugLine(GetWorld(), WallClimbRayStart, WallRunRayEndRight, FColor::Blue, 0, 2);
	if (DEBUGENABLED) UKismetSystemLibrary::DrawDebugLine(GetWorld(), WallClimbRayStart, WallRunRayEndLeft, FColor::Yellow, 0, 2);

	FRotator ActorRot = GetOwner()->GetActorRotation();

	switch (PlayerState)
	{
	case EPlayerState::IDLE:
		print("IDLE", CurrentTime);

		if (GetWorld()->GetFirstPlayerController()->GetPawn()->GetControlRotation().Roll != 0)
			GetWorld()->GetFirstPlayerController()->SetControlRotation(FRotator(GetWorld()->GetFirstPlayerController()->GetPawn()->GetControlRotation().Pitch, GetWorld()->GetFirstPlayerController()->GetPawn()->GetControlRotation().Yaw, 0));
		CharacterMovementComponent->GravityScale = 1;
		CharacterMovementComponent->SetPlaneConstraintNormal(FVector(0, 0, 0));

		break;
	case EPlayerState::WALL_CLIMBING:
		print("WALL_CLIMBING", CurrentTime);
		if (GetWorld()->GetFirstPlayerController()->GetPawn()->GetControlRotation().Roll != 0)
			GetWorld()->GetFirstPlayerController()->SetControlRotation(FRotator(GetWorld()->GetFirstPlayerController()->GetPawn()->GetControlRotation().Pitch, GetWorld()->GetFirstPlayerController()->GetPawn()->GetControlRotation().Yaw, 0));
		ClimbWall();

		break;
	case EPlayerState::WALL_RUNNING_R:
		print("WALL_RUNNING_R", CurrentTime);
		RunWall(0);
		break;
	case EPlayerState::WALL_RUNNING_L:
		print("WALL_RUNNING_L", CurrentTime);
		RunWall(1);
		break;
	default:
		break;
	}

}


void UCustomMovementComponent::OnDash()
{
	if (!CanDash)
		return;
	//CharacterMovementComponent->StopMovementImmediately();
	CharacterMovementComponent->AddImpulse(Cast<AFP_FirstPersonCharacter>(GetOwner())->GetFirstPersonCameraComponent()->GetForwardVector() * DashForce, true);
	print("Dash!", 2);
	CanDash = false;
}

void UCustomMovementComponent::ResetJump()
{
	CurrentJumpCount = 0;
}

void UCustomMovementComponent::ResetDash()
{
	CanDash = true;
}

void UCustomMovementComponent::CustomJump()
{
	switch (CurrentJumpCount)
	{
	case 0:
		OwnerCharacter->LaunchCharacter(FVector(0, 0, JumpForce), 0, 1);
		++CurrentJumpCount;
		break;
	case 1:
		OwnerCharacter->LaunchCharacter(FVector(0, 0, JumpForce), 0, 1);
		++CurrentJumpCount;
		break;


	}

}

void UCustomMovementComponent::ClimbWall()
{
	OwnerCharacter->LaunchCharacter(FVector(0, 0, ClimbForce), 1, 1);

}

void UCustomMovementComponent::RunWall(int32 direction)
{

	FRotator ActorRot = GetOwner()->GetActorRotation();
	CharacterMovementComponent->SetPlaneConstraintNormal(FVector(0, 0, 1));
	CharacterMovementComponent->GravityScale = 0;
	CharacterMovementComponent->AddForce(GetOwner()->GetActorForwardVector() * WallRunForce);
	CharacterMovementComponent->Velocity.Z = 0;

	if (direction == 0)
		OnWallRunningR.Broadcast();
	else
		OnWallRunningL.Broadcast();


}


void UCustomMovementComponent::JumpEnd()
{
	OnNormalizeView.Broadcast();

	switch (PlayerState)
	{
	case EPlayerState::IDLE:
		break;
	case EPlayerState::WALL_CLIMBING:
		OwnerCharacter->LaunchCharacter(OwnerCharacter->GetActorForwardVector() * -JumpAwayFromWallForce, 1, 1);
		break;
	case EPlayerState::WALL_RUNNING_L:
		OwnerCharacter->LaunchCharacter(OwnerCharacter->GetActorRightVector() * JumpAwayFromWallForce + OwnerCharacter->GetActorForwardVector() * JumpAwayFromWallForce, 1, 1);
		break;
	case EPlayerState::WALL_RUNNING_R:
		OwnerCharacter->LaunchCharacter(OwnerCharacter->GetActorRightVector() * -JumpAwayFromWallForce + OwnerCharacter->GetActorForwardVector() * JumpAwayFromWallForce, 1, 1);
		break;
	default:
		break;
	}

}



